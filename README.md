# multiedro

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### e-mail e senha (ADMIN)
```
e-mail: useradm@teste.com.br
senha: 12345678
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
