import Vue from "vue";
import VueRouter from "vue-router";
import Login from "@/components/Login.vue";
import Register from "@/components/Register.vue";
import Home from "@/components/Home.vue";
import Departament from "@/views/departament/Index.vue";
import FormDep from "@/views/departament/Form.vue";
import Users from "@/views/users/Index.vue";
import FormUser from "@/views/users/Form.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "login",
    component: Login
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/register",
    name: "register",
    component: Register,
    meta: {
      permission: "any"
    }
  },
  {
    path: "/home",
    name: "home",
    component: Home
  },
  {
    path: "/departaments",
    name: "departaments",
    component: Departament
  },
  {
    path: "/departament-form",
    name: "departamentForm",
    component: FormDep
  },
  {
    path: "/users",
    name: "users",
    component: Users
  },
  {
    path: "/user-form/:id",
    name: "userForm",
    component: FormUser
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
