import firebase from "firebase";

let config = firebase.initializeApp({
  apiKey: "AIzaSyCXPCtXgReNg_mBqjRHqaQH4WiKuCTLczE",
  authDomain: "teste-multiedro.firebaseapp.com",
  databaseURL: "https://teste-multiedro.firebaseio.com",
  projectId: "teste-multiedro",
  storageBucket: "teste-multiedro.appspot.com",
  messagingSenderId: "28184066798",
  appId: "1:28184066798:web:75cde345a6fe404ff251ff"
});

const auth = config.auth();
const db = config.firestore();

export default {
  auth,
  db
};
