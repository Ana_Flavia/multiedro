export default {
  errorCode(code) {
    if (code === "auth/wrong-password")
      alertSweet("Senha incorreta", "Digite uma senha valida.");
    else if (code === "auth/user-not-found")
      alertSweet("Usuário não encontrado", "E-mail ou senha inválido.");
    else if (code === "auth/email-already-in-use") {
      alertSweet("E-mail já cadastrado", "Informe outro e-mail válido.");
    } else if (code === 400) {
      alertSweet(
        "E-mail não encontrado",
        "Caso não possua cadastro, crie uma conta."
      );
    }
  }
};

function alertSweet(title, msg) {
  swal(title, msg, "error");
}
