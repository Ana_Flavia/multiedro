import Vue from "vue";
import Vuex from "vuex";
import Login from "@/store/modules/Login";
import User from "@/store/modules/User";
import Departament from "@/store/modules/Departament";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    login: Login,
    departament: Departament,
    user: User
  }
});
