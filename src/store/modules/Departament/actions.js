import firebase from "@/services/firebase-service.js";

export default {
  async getListDepartaments({ dispatch }) {
    let departaments = await firebase.db.collection("departamentos").get();

    return departaments.docs.map(doc => doc.data());
  },
  setNewDepartament({ dispatch }, departament) {
    let ref = firebase.db.collection("departamentos").doc();

    departament.id = ref.id;

    ref.set(departament);
  },
  async getDepartament({ dispatch }, id) {
    return await firebase.db
      .collection("departamentos")
      .doc(id)
      .get()
      .then(doc => {
        return doc.data();
      });
  },
  deleteDepartament({ dispatch }, id) {
    firebase.db
      .collection("departamentos")
      .doc(id)
      .delete();
  },
  updateDepartamentExists({ dispatch }, departament) {
    firebase.db
      .collection("departamentos")
      .doc(departament.id)
      .update(departament);
  }
};
