export const mutations = {
  SET_NEW_DEPARTAMENT: "SET_NEW_DEPARTAMENT",
  SET_EXIST_DEPARTAMENT: "SET_EXIST_DEPARTAMENT",
  GET_LIST_DEPARTAMENT: "GET_LIST_DEPARTAMENT",
  DELETE_DEPARTAMENT: "DELETE_DEPARTAMENT"
};

export default {
  [mutations.SET_NEW_DEPARTAMENT]: (state, payload) =>
    (state.departament = payload),
  [mutations.SET_EXIST_DEPARTAMENT]: (state, payload) =>
    (state.departament = payload),
  [mutations.GET_LIST_DEPARTAMENT]: (state, payload) =>
    (state.listDepartaments = payload),
  [mutations.DELETE_DEPARTAMENT]: (state, payload) =>
    (state.departament = payload)
};
