import firebase from "@/services/firebase-service.js";

export default {
  logIn({}, credentials) {
    const { email, password } = credentials;

    return firebase.auth.signInWithEmailAndPassword(email, password).then(
      response => {
        return response;
      },
      error => {
        return error;
      }
    );
  },
  async logOut({ commit }) {
    return await firebase.auth.signOut().then(
      response => {
        return response;
      },
      error => {
        return error;
      }
    );
  },
  createUserAuthFirebase({ dispatch }, credentials) {
    return firebase.auth
      .createUserWithEmailAndPassword(credentials.email, credentials.password)
      .then(
        response => {
          dispatch("logIn", credentials);
          return response;
        },
        error => {
          return error;
        }
      );
  },
  deleteUserAccountLogin({ state }) {
    firebase.auth.currentUser.delete();
  }
};
