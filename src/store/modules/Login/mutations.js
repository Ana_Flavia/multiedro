export const mutations = {
  STATUS_CURRENT_USER: "STATUS_CURRENT_USER"
};

export default {
  [mutations.STATUS_CURRENT_USER]: (state, payload) => (state.logged = payload)
};
