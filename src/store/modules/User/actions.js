import firebase from "@/services/firebase-service.js";

export default {
  async getListUsers() {
    let users = await firebase.db.collection("funcionarios").get();

    return users.docs.map(doc => doc.data());
  },
  setNewUser({ dispatch }, user) {
    let ref = firebase.db.collection("funcionarios").doc();

    user.id = ref.id;

    ref.set(user);
  },
  async getUser({ dispatch }, id) {
    return await firebase.db
      .collection("funcionarios")
      .doc(id)
      .get()
      .then(doc => {
        return doc.data();
      });
  },
  async getUserByEmail({ state }, email) {
    return await firebase.db
      .collection("funcionarios")
      .where("email", "==", email)
      .get()
      .then(snapshot => {
        if (!snapshot.empty) {
          snapshot.docs.map(doc => {
            if (doc.data().isAdmin) state.isAdmin = true;
            else state.isAdmin = false;
          });
        }
        return;
      });
  },
  deleteUser({ dispatch }, id) {
    firebase.db
      .collection("funcionarios")
      .doc(id)
      .delete();
  },
  updateUserExists({ dispatch }, user) {
    firebase.db
      .collection("funcionarios")
      .doc(user.id)
      .update(user);
  }
};
