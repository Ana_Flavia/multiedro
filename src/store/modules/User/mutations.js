export const mutations = {
  SET_NEW_USER: "SET_NEW_USER",
  SET_EXIST_USER: "SET_EXIST_USER",
  GET_LIST_USERS: "GET_LIST_USERS",
  DELETE_USER: "DELETE_USER"
};

export default {
  [mutations.SET_NEW_USER]: (state, payload) => (state.user = payload),
  [mutations.SET_EXIST_USER]: (state, payload) => (state.user = payload),
  [mutations.GET_LIST_USERS]: (state, payload) => (state.listUsers = payload),
  [mutations.DELETE_USER]: (state, payload) => (state.user = payload)
};
