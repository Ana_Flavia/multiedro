export default {
  listUsers: [],
  user: {
    id: Number,
    name: String,
    email: String,
    departament: String,
    isAdmin: Boolean
  },
  isAdmin: false
};
